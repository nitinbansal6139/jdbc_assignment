create table employee(userId int, firstName varchar(255), lastName varchar(255), email varchar(255));

insert into employee (userId, firstName, lastName, email) values (?, ?, ?, ?)

update employee set firstName = ?, lastName = ?, email = ? where userId = ?

select * from employee

delete from employee where userId = ?
package com.greatlearning.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EmployeeDAO {

    public void addEmployee(EmployeeModel empDTO) throws SQLException {
        Connection conn = FetchConnection.getInstance();
        String query = "insert into employee (userId, firstName, lastName, email) values (?, ?, ?, ?)";
        try (PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, empDTO.getUserId());
            statement.setString(2, empDTO.getFirstName());
            statement.setString(3, empDTO.getLastName());
            statement.setString(4, empDTO.getEmailID());
            statement.executeUpdate();
            System.out.println("User has been inserted");
        }
    }

    public void updateEmployee(EmployeeModel empDTO) throws SQLException {
        Connection conn = FetchConnection.getInstance();
        String query = "update employee set firstName = ?, lastName = ?, email = ? where userId = ?";
        try (PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setString(1, empDTO.getFirstName());
            statement.setString(2, empDTO.getLastName());
            statement.setString(3, empDTO.getEmailID());
            statement.setInt(4, empDTO.getUserId());
            if (statement.executeUpdate() == 0)
                System.out.println("No employee found with user id " + empDTO.getUserId());
            else
                System.out.println("User has been updated");
        }
    }

    public void displayAllEmployees() throws SQLException {
        Connection conn = FetchConnection.getInstance();
        String query = "select * from employee";
        try (Statement statement = conn.createStatement(); ResultSet rs = statement.executeQuery(query)) {
            while (rs.next()) {
                EmployeeModel empDTO = new EmployeeModel(rs.getInt("userid"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("email"));
                System.out.println(empDTO);
            }
        }
    }

    public void deleteEmployee(int userId) throws SQLException {
        Connection conn = FetchConnection.getInstance();
        String query = "delete from employee where userId = ?";
        try (PreparedStatement statement = conn.prepareStatement(query)) {
            statement.setInt(1, userId);
            if (statement.executeUpdate() == 0)
                System.out.println("No user found with user id " + userId);
            else
                System.out.println("User has been deleted");
        }
    }


}

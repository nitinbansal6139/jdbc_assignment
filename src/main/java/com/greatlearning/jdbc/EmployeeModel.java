package com.greatlearning.jdbc;

public class EmployeeModel {
    private final int userId;
    private final String firstName;
    private final String lastName;
    private final String emailID;

    public EmployeeModel(int userId, String firstName, String lastName, String emailID) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailID = emailID;
    }

    public int getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailID() {
        return emailID;
    }

    @Override
    public String toString() {
        return "{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailID='" + emailID + '\'' +
                '}';
    }
}

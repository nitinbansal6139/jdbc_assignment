package com.greatlearning.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class FetchConnection {
    private static Connection conn = null;

    private FetchConnection() {
    }

    public static Connection getInstance() throws SQLException {
        if (conn == null) {
            createConnection();
        }
        return conn;
    }

    private static void createConnection() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/empdb", "root", "root");
    }

    public static void releaseConnection() throws SQLException {
        if (conn != null && !conn.isClosed()) {
            conn.close();
        }
    }
}

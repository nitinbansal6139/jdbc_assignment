package com.greatlearning.jdbc;

import java.sql.SQLException;
import java.util.Scanner;

public class JDBCMain {
    public static void main(String[] args) throws SQLException {
        System.out.println("!! Welcome to the User CRUD Services !!");
        System.out.println("-------------------------------------");
        performJDBCOperations();
    }

    public static void performJDBCOperations() throws SQLException {
        EmployeeDAO empDAO = new EmployeeDAO();

        try (Scanner jdbcScanner = new Scanner(System.in)) {
            int jdbcInput;
            int userId;
            String firstName;
            String lastName;
            String email;
            do {
                System.out.println("Enter the operation that you want to perform");
                System.out.println("1. Registration");
                System.out.println("2. Update");
                System.out.println("3. Display Data");
                System.out.println("4. Delete");
                System.out.println("0. Exit");
                jdbcInput = jdbcScanner.nextInt();
                switch (jdbcInput) {
                    case 1:
                        System.out.println("Enter user id");
                        userId = jdbcScanner.nextInt();
                        System.out.println("Enter first name");
                        firstName = jdbcScanner.next();
                        System.out.println("Enter last name");
                        lastName = jdbcScanner.next();
                        System.out.println("Enter email");
                        email = jdbcScanner.next();
                        empDAO.addEmployee(new EmployeeModel(userId, firstName, lastName, email));
                        break;
                    case 2:
                        System.out.println("Enter user id for which employee you want to update");
                        userId = jdbcScanner.nextInt();
                        System.out.println("Enter first name to update");
                        firstName = jdbcScanner.next();
                        System.out.println("Enter last name to update");
                        lastName = jdbcScanner.next();
                        System.out.println("Enter email to update");
                        email = jdbcScanner.next();
                        empDAO.updateEmployee(new EmployeeModel(userId, firstName, lastName, email));
                        break;
                    case 3:
                        empDAO.displayAllEmployees();
                        break;
                    case 4:
                        System.out.println("Enter user Id");
                        userId = jdbcScanner.nextInt();
                        empDAO.deleteEmployee(userId);
                        break;
                    case 0:
                        break;
                    default:
                        System.out.println("Input should be 1-4");
                        break;
                }
            } while (jdbcInput != 0);
        } finally {
            FetchConnection.releaseConnection();
        }
    }
}
